<?php
/**
 * @author Balance Internet Team
 * @copyright Copyright (c) 2016 Balance Internet Pty (http://www.balanceinternet.com.au)
 * @package Balance_Cms
 */

namespace Balance\Cms\Setup;

use Magento\Cms\Model\Page;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

use Magento\Cms\Model\Block;
use Magento\Cms\Model\BlockFactory;
use Magento\Store\Model\Store;
use \Magento\Framework\File\Csv;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var \Magento\Framework\File\Csv
     */
    protected $csvReader;

    /**
     * @param PageFactory $pageFactory
     * @param BlockFactory $blockFactory
     * @param Csv $csvReader
     */
    public function __construct(PageFactory $pageFactory, BlockFactory $blockFactory, Csv $csvReader)
    {
        $this->pageFactory = $pageFactory;
        $this->blockFactory = $blockFactory;
        $this->csvReader = $csvReader;
    }

    /**
     * Create page
     *
     * @return Page
     */
    private function createPage()
    {
        return $this->pageFactory->create();
    }

    /**
     * Create block
     *
     * @return Block
     */
    private function createBlock()
    {
        return $this->blockFactory->create();
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if ($context->getVersion()) {
            /* Directory separator */
            $ds = DIRECTORY_SEPARATOR;

            $fixtureFolder = dirname(dirname(__FILE__)) .$ds. 'fixtures';

            $versions = glob($fixtureFolder .$ds. "*", GLOB_ONLYDIR);
            if (count($versions)) {
                foreach ($versions as $version) {
                    $_version = basename($version);
                    if (is_numeric(str_replace('.', '', $_version))) {
                        if (version_compare($context->getVersion(), $_version, '<')) {
                            /* Insert / update blocks */
                            $rows = $this->csvReader->getData($version .$ds. 'blocks.csv');
                            $header = array_shift($rows);

                            foreach ($rows as $row) {
                                $data = [];
                                foreach ($row as $key => $value) {
                                    $data[$header[$key]] = $value;
                                }
                                $row = $data;
                                if (!empty($row['content_file'])) {
                                    $row['content'] = file_get_contents($version .$ds. 'blocks' .$ds. $row['content_file']);
                                    unset($row['content_file']);
                                }

                                $this->blockFactory->create()->load($row['identifier'], 'identifier')
                                    ->addData($row)
                                    ->setStores([Store::DEFAULT_STORE_ID])
                                    ->save();
                            }

                            /* Insert / update pages */
                            $rows = $this->csvReader->getData($version .$ds. 'pages.csv');
                            $header = array_shift($rows);

                            foreach ($rows as $row) {
                                $data = [];
                                foreach ($row as $key => $value) {
                                    $data[$header[$key]] = $value;
                                }
                                $row = $data;
                                if (!empty($row['content_file'])) {
                                    $row['content'] = file_get_contents($version .$ds. 'pages' .$ds. $row['content_file']);
                                    unset($row['content_file']);
                                }
                                $this->pageFactory->create()
                                    ->load($row['identifier'], 'identifier')
                                    ->addData($row)
                                    ->setStores([Store::DEFAULT_STORE_ID])
                                    ->save();
                            }
                        }
                    }
                }
            }
        }

        $setup->endSetup();
    }
}
