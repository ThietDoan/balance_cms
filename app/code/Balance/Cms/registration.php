<?php
/**
 * @author Balance Internet Team
 * @copyright Copyright (c) 2016 Balance Internet Pty (http://www.balanceinternet.com.au)
 * @package Balance_Cms
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Balance_Cms', __DIR__);
